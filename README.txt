	Dear Ian

	I started by creating the Records and Tables classes. Database is my main class where almost everything gets called.
	 Then I printed them out  as it helped me debuq and to visualise my progres. I struggled quite alot trying to call,
	  fill, erase and handle the list from Records but it felt good to understand it. 
	   Then I added the Primary Key whitch is the "ID" column at the begining of every row , it is Unique and
	    AUTO-Increments and finally, created the Files class where I save and load ".txt" files. Handling files from a
	     separate class was challenging as well(I include a "example.txt" file. I also created an Enum class to learn to
	      handle Enums as well as defines. I did use symbols as suggested. Finally I tried to make more comments as
	       requested from the OXO feedback. 

 	-Extension
	While making the Primary Key I decided to make a user Interface as I already had most of my options printed out 
	while I was de-buging!

	-Testing
	I tested all the functions that I could. Most of them could not be tested as they depended on scanner input!!! 
	I tried JUnit for the first time because it's industrial standard.

	-S.O.S. BUGS!!!
	Everything works fine but for some reason it may throw messages as "Wrong number..." or "Wrong column name" EVEN if
	 the commands WORK and every change has been SAVED on the database! Check the "Print tables" option to verify.
For some reason every time I closed the scanner my program would crash at some point so I commented it out!

	Type: gradle run & gradle test

	
