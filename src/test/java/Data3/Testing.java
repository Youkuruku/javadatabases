package test.java.Data3;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import main.java.Data3.Databases;
import main.java.Data3.Files;
import main.java.Data3.Records;
import main.java.Data3.Tables;

public class Testing {
	
	Databases db = new Databases();
	Files file = new Files();
	
	//this test method used to work, until I had to call the 
	//`originalMessage` in it, which requires scanner input
	@Test
	@Ignore
	public void deleteSelectedTableTest(){
		Tables t1 = new Tables();
		Tables t2 = new Tables();
		t1.setName("table1");
		t2.setName("table2");
		db.getTables().add(t1);
		db.getTables().add(t2);
		assertEquals(2, db.getTables().size());
		assertFalse(db.deleteSelectedTable("wrong name"));
		assertTrue(db.deleteSelectedTable("table1"));
		assertEquals(2, db.getTables().size());
	}
	
	@Test
	public void fillDatabaseTest(){
		Databases d = new Databases();
		List<String> tablesInList = createTableForTest();
		file.fillDatabase(d, tablesInList);
		
		assertFalse(d.getTables().isEmpty());
		assertEquals( 2, d.getTables().size());
		assertTrue(d.getTables().get(0).getName().equals("emptyTable"));
	}
	
	@Test
	public void findColumndStringTest(){
		List<String> tablesInList = createTableForTest();
		List<String> columns = file.findColumnStrings(3, tablesInList);
		
		assertFalse(columns.isEmpty());
		assertEquals(1, columns.size());
		assertTrue(columns.get(0).equals("dummyColumn"));

		columns = file.findColumnStrings(7, tablesInList);

		assertEquals(2, columns.size());
		assertTrue(columns.get(1).equals("surname"));

	}
	
	@Test
	public void convertLstToString(){
		assertTrue(file.convertListToStrings(createTableForTest())
				.equals("! emptyTable £ dummyColumn ! member £ name surname "
						+ "@ jordan perdikidis ")); 
	}
	
	@Test
	public void createRecordTest(){
		Records r = Tables.createRecord(10,  createTableForTest());
		assertFalse(r==null);
		assertEquals(r.getTuples().size(), 2);
		assertTrue(r.getTuples().get(0).equals("jordan"));
	}
	
	@Test
	public void reateNewTableTest(){
		Databases d = new Databases();
		assertTrue(d.getTables().isEmpty());
		d.createNewTable();
		assertFalse(d.getTables().isEmpty());
		assertEquals(1, d.getTables().size());
		
	}
	
	private List<String> createTableForTest(){
		List<String> l = new ArrayList<>();
		l.add("!");
		l.add("emptyTable");
		l.add("£");
		l.add("dummyColumn");
		l.add("!");
		l.add("member");
		l.add("£");
		l.add("name");
		l.add("surname");
		l.add("@");
		l.add("jordan");
		l.add("perdikidis");
		return l;
	}
}
