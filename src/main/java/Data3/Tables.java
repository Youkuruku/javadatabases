package main.java.Data3;

import java.util.ArrayList;
import java.util.List;


public class Tables {

	private String name;
	private List<String> columns = new ArrayList<String>();
	private List<Records> rows = new ArrayList<Records>();
	private int counter ;
	
	

	
	public Tables() {
		counter = 1;
	}

	//this method is for the Files class
	public static Records createRecord(int index, List<String> fileContent){
		Records recordsobject = new Records();
		for(int i = index; i< fileContent.size(); i++){
			if(!fileContent.get(i).equals(Symbols.RECORDSYMBOL.getText()) &&
               !fileContent.get(i).equals(Symbols.TABLESYMBOL.getText())){
				recordsobject.getTuples().add(fileContent.get(i));
			}
			else{
				break;
			}
		}		
		return recordsobject;
	}
	
	public int getCounter() {
		return counter;
	}


	public void setCounter(int counter) {
		this.counter = counter;
	}


	public void createTable(){
		
//		records.startArray();
//		rows.add(records);
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public List<String> getColumns() {
		return columns;
	}



	public void setColumns(List<String> columns) {
		this.columns = columns;
	}



	public List<Records> getRows() {
		return rows;
	}



	public void setRows(List<Records> rows) {
		this.rows = rows;
	}
	
	

}
