package main.java.Data3;

import java.util.*;
//import java.time.Year;
import java.util.ArrayList;
//import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
//
//import javax.naming.directory.SchemaViolationException;
//import javax.xml.ws.handler.MessageContext;
//
//import org.omg.CORBA.PRIVATE_MEMBER;


/**
 * @author jordan
 *
 */
public class Databases {
	
	private final static char DUMMYWRONGINPUT = 'q';
	private final static int RANDOMNUMBER = 100000;
	private List<Tables> tables = new ArrayList<Tables>();
	
	
	public static void main(String args[]){
		Databases databasesobject = new Databases();
		databasesobject.originalMessage();
	}

	public void createNewTable(){
		Tables t = new Tables();
		tables.add(t);
	}

	private void createTable(){
		
		Tables tablesobject = new Tables();
		Scanner scannerobject = new Scanner(System.in);
		System.out.println("Create your table. Enter table name:");
		String tableName = scannerobject.nextLine();
		tablesobject.setName(tableName);//put name in tables class
		System.out.println("Enter the desired column names seperated by space. Be carefull with the use of spaces!!!");
		String columnsNames = scannerobject.nextLine();
		String[] columnArray = columnsNames.split(" ");//separate into individual strings at space
		tablesobject.getColumns().add("ID");//create ID column at slot[0]
			for(String s : columnArray){//add strings to Tables.columns
				tablesobject.getColumns().add(s);
			}
		// I could not use this line because then I can't add a new column later on in my code!!
		//tablesobject.setColumns(Arrays.asList(columnsNames.split(" ")));
		System.out.println("Do you wish to enter a Record? Press Y or N");
		String userInput;
		char choice;
		do{
			userInput = scannerobject.next();//make sure that input is 1 character long
			if (userInput.length() != 1) {// i do this so Y will pass but Yes will NOT pass!
				choice = DUMMYWRONGINPUT;
			}
			else{
				choice = userInput.charAt(0);
			}
			if(Character.toUpperCase(choice) !='Y' && Character.toUpperCase(choice)!= 'N'){
				System.out.println("Wrong character! Try again: Y or N");
			}
		}while(Character.toUpperCase(choice) !='Y' && Character.toUpperCase(choice)!= 'N');
		if(Character.toUpperCase(choice)== 'Y' ){
			addRecord(tablesobject);
		}
		tables.add(tablesobject);
		originalMessage();
//        scannerobject.close();
	}
	
	private void addRecord(Tables tablesobject){
		Scanner scannerobject = new Scanner(System.in);
		Records recordsobject = new Records();
		int columnsNumber = tablesobject.getColumns().size() - 1;//minus 1 because i added the Id column
		System.out.println("Enter "+columnsNumber+" values for your Record seperated by space");
		String values = scannerobject.nextLine(); 
		String[] valuesArray = values.split(" ");//separate at space
		int valueArraySize = valuesArray.length;//check if correct num of strings is entered
		if(valueArraySize == columnsNumber){
		    int counter = tablesobject.getCounter();
			recordsobject.getTuples().add("" + counter++);// AUTO-INCREMENT  primary key in ID column
			for(String s : valuesArray){
				recordsobject.getTuples().add(s);//add strings to my records object
			}
			tablesobject.setCounter(counter);
		tablesobject.getRows().add(recordsobject);
	    System.out.println("Do you wish to enter another record? Y or N?");
		char choice2;
		do{
			choice2 = scannerobject.next().charAt(0);
			if(Character.toUpperCase(choice2) !='Y' && Character.toUpperCase(choice2)!= 'N'){
				System.out.println("Wrong character! Try again: Y or N");
			}
		}while(Character.toUpperCase(choice2) !='Y' && Character.toUpperCase(choice2)!= 'N');
		// If i close the scanner it throws an error
		//		scannerobject.close();
			if(Character.toUpperCase(choice2)== 'Y' ){
				addRecord(tablesobject);
			}
		}else{
			System.out.println("You need to enter excactly "+columnsNumber+" values!!!");
			addRecord(tablesobject);}	
	}
	
	private void printTables(){
		if(!tables.isEmpty()){
			for(Tables t  : tables){
				printSingleTable(t);
			}
			originalMessage();
		}else{
			System.out.println("There are no tables currently in the database.\n");
			originalMessage();
		}	
	}
	
	private void printSingleTable(Tables t){
		System.out.printf("%5s\n",t.getName());//put table name to the right to look better
		System.out.println(t.getColumns());
		for( Records r: t.getRows()){
			System.out.println(r.getTuples());
		}
		System.out.print("\n");
	}
	
	private void originalMessage(){
		System.out.println("What is your wish o mighty programmer ???"
		+"\n"+"Select 1 to create a new table"
		+"\n"+"Select 2 to edit an existing table"
		+"\n"+"Select 3 to delete a  table"
		+"\n"+"Select 4 to save the data to a file"
		+"\n"+"Select 5 to load  data from a file"
		+"\n"+"Select 6 to print the tables"
		+"\n"+"Select 7 to exit the database");
		Scanner scannerobject = new Scanner(System.in);
		String userSelection = scannerobject.nextLine();
		selectMethod(tryparse(userSelection));
//        scannerobject.close();

	}
	
	private void selectMethod( int selection){
		if(selection == 1 ){createTable();}
		if(selection == 2 ){editTable();}
		if(selection == 3 ){deleteTable();}
		if(selection == 4 ){saveToFile();}
		if(selection == 5 ){loadFromFile();}
		if(selection == 6 ){printTables();}
		if(selection == 7 ){exitDatabase();}
		
		else{
			System.out.println("Wrong number! Try again...\n");
			originalMessage();}
	}
	
	private void loadFromFile(){
		Files filesobject = new Files();
		filesobject.readFile(this);

		
	}
	
	private void saveToFile(){
		Files filesobject = new Files();
		filesobject.createFile(this.tables);
		
	}
	
	private void deleteTable(){
		System.out.println("Which table do you want to delete?");
		Scanner scannerobject = new Scanner(System.in);
		String selectDeleteTable = scannerobject.nextLine();
		deleteSelectedTable(selectDeleteTable);
		if(!deleteSelectedTable(selectDeleteTable)){
			System.out.println("Wrong table name. Try again. Check if table exists..\n");
			originalMessage();				
		}
//        scannerobject.close();
	}
	
	public boolean deleteSelectedTable(String selectDeleteTable){
		for(Tables t : tables){
			if(t.getName().equals(selectDeleteTable)){
				tables.remove(t);
				System.out.println("The table has been succesfully deleted!\n");
				originalMessage();
				return true;
			}
		}
		return false;
	}
	
	private void editTable(){
		System.out.println("Whitch table do you wish to edit?");
		Scanner scannerobject = new Scanner(System.in);
		String selectEditTable = scannerobject.nextLine();
		
		for(Tables t : tables){
			if(t.getName().equals(selectEditTable)){
				int userSelection = editMessage();
				editChoice(userSelection, t);	
			}
		}
		for(Tables t : tables){
			if(!t.getName().equals(selectEditTable)){
				System.out.println("Wrong table name. Plz try again");
				editTable();
			}
		}	
//        scannerobject.close();
	}
	
	private void editChoice(int userSelection, Tables t){
		if(userSelection == 1){addRecord(t);}
		if(userSelection == 2){deleteRecord(t);}
		if(userSelection == 3){addColumn(t);}
		if(userSelection == 4){deleteColumn(t);}
		if(userSelection == 5){changeTableName(t);}
		else{System.out.println("Wrong number. Try again...\n");}
		
		originalMessage();
	}
	
	private void deleteRecord(Tables t){
		System.out.println("Which row number do you wish to delete?? Enter row number");
		Scanner scannerobject = new Scanner(System.in);
		String userSelection = scannerobject.nextLine();
		int rowNumber = tryparse(userSelection);
		if(rowNumber <= t.getRows().size()){
		selectRowToDelete(rowNumber, t);
		System.out.println("Row "+rowNumber+" has been succesfully deleted\n");
		originalMessage();
		}
		else{
			System.out.println("This row number does not exist. Try again. Maybe "
			+"check the tables rows again."		);
		}
//        scannerobject.close();
	}
	
	private void selectRowToDelete(int rowNumber, Tables t){
		t.getRows().remove(rowNumber);
	}
	
	
	
	private int editMessage(){
		System.out.println("What do you wish to edit??"+"\n"
				+"Select 1 to add a row"+"\n"
				+"Select 2 to delete a row"+"\n"
				+"Select 3 to add a column"+"\n"
				+"Select 4 to delete a column"+"\n"
				+"Select 5 to change table's name"+"\n");
		Scanner scannerobject = new Scanner(System.in);
		String x = scannerobject.nextLine();
		int userSelection = tryparse(x);
//        scannerobject.close();
		return userSelection;
	}
	
	private int tryparse(String userSelection){
		try {
		    return Integer.parseInt(userSelection);
		  } catch (NumberFormatException e) {
		    return RANDOMNUMBER;//random magic number
		  }
	}
	
	private void addColumn(Tables t){
		System.out.println("What is the new column's name?\n");
		Scanner scannerobject = new Scanner(System.in);
		String newName = scannerobject.nextLine();
		t.getColumns().add(newName);
		System.out.println("Column has been succesfully added\n");
		originalMessage();
//        scannerobject.close();
	}
	
	private void deleteColumn(Tables t){
		int columnPlace;
		System.out.println("Which column NAME do you wish to delete?\n");
		Scanner scannerobject = new Scanner(System.in);
		String columnName = scannerobject.nextLine();
		//I have to do a Iterator while loop, because for loop throws an error
		//if I modify a collection while iterating over it.
		final Iterator<String> iterator = t.getColumns().iterator();
		while (iterator.hasNext()) {
		    final Object o = iterator.next();
		    if(o.equals(columnName)==false){
		    	System.out.println("Wrong column name...Check the table name and try again.\n");
		    }
		    if(o.equals("ID")){
		    	System.out.println("Can't delete the primary key column");
		    }
		    if (o.equals(columnName)){
		        columnPlace = t.getColumns().indexOf(columnName);//find the position of the string
		    	iterator.remove();
		    	for(Records r : t.getRows()){
		    		deleteRowElement(columnPlace, r);
		    	}
		    	System.out.println("Column has been succesfully deleted\n");		    	
		    }
		}
		originalMessage();
//        scannerobject.close();
	}
	
	private void deleteRowElement(int columnPlace, Records r){
		r.getTuples().remove(columnPlace);
	}
	
	private void changeTableName(Tables t){
		System.out.println("What is the new name of the table?");
		Scanner scannerobject = new Scanner(System.in);
		String newName = scannerobject.nextLine();
		t.setName(newName);
		System.out.println("The table's name has been succesfully changed\n");
		originalMessage();
//        scannerobject.close();
	}
	
	private void exitDatabase(){
		System.out.println("Thank you for selecting our database services.\n"
		+"For security reasons the database and it's records will self-destruct..."		
		+" Have a nice day!!!");
		System.exit(0);
	}


	public List<Tables> getTables() {
		return tables;
	}


	public void setTables(List<Tables> tables) {
		this.tables = tables;
	}
	
	
	
}