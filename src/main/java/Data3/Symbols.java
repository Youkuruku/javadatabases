package main.java.Data3;

public enum Symbols {	
    TABLESYMBOL("!"),
    COLUMNSYMBOL("£"),
    RECORDSYMBOL("@");

    private final String text;

    /**
     * @param text
     */
    private Symbols(final String text) {
        this.text = text;
    }

    public String getText() {
		return text;
	}
//    /* (non-Javadoc)
//     * @see java.lang.Enum#toString()
//     */
//    @Override
//    public String toString() {
//        return text;
//    }

}
