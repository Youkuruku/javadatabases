package main.java.Data3;

import java.util.ArrayList;
import java.io.*;
import java.lang.reflect.Array;
import java.util.List;
import java.util.Scanner;
//
//import javax.swing.event.AncestorEvent;
//
//import org.junit.experimental.theories.Theories;
//import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

public class Files {
	//Tables tablesobject = new Tables();
	
	
	public void createFile(List<Tables> tables){
		List<String> newFile = new ArrayList<String>();
		 for(Tables t : tables){
			 newFile.add(Symbols.TABLESYMBOL.getText() + " " +t.getName() + "\n");
			 newFile.add(Symbols.COLUMNSYMBOL.getText() + " " + convertListToStrings(t.getColumns()) + "\n");
			 for(Records r : t.getRows()){
				 newFile.add(Symbols.RECORDSYMBOL.getText() + " " + convertListToStrings(r.getTuples()) + "\n");
			 }
			 newFile.add("\n");//add empty line after each table
			 //Used print for de-bugging
			 //printSavedFile(newFile);
		 }
		 SaveToFile(newFile);
	}
	
	private void SaveToFile(List<String> newFile){
		try{
			System.out.println("What is the File name???");
			Scanner scannerobject = new Scanner(System.in);
			String fileName = scannerobject.next();
	        File fileobject = new File (fileName+".txt");
	        PrintWriter printWriterobject = new PrintWriter(fileobject);
	        for (String s : newFile) {
	        	printWriterobject.print(s);	
			}
//	        scannerobject.close();
	        printWriterobject.close();
		}
		catch (FileNotFoundException ex) {
	         System.out.println(ex);
	      }
		System.out.println("Your file has been succesfully saved!!");
		
	}
	
	//put files content in a new list of strings
	public void readFile(Databases d ){
		System.out.println("Current Files in directory:\n");
		filesInDirectory();
		try{
			System.out.println("Which file to you wish to read? Don't forget the .txt part!");
			Scanner scannerobject = new Scanner(System.in);
			String fileName = scannerobject.next();
			File fileRead = new File(fileName);
	         List<String> fileContent = new ArrayList<String>();
	         Scanner scannerobject2 = new Scanner(fileRead);
	         while (scannerobject2.hasNext()) {
	             String x = scannerobject2.next();
	             fileContent.add(x);        
	          }
//	         if I close The scanners It throws  error for some reason
//	         scannerobject.close();
//	         scannerobject2.close();
	         fillDatabase(d, fileContent);
	         	
		}
		// used 2 catches because i must understand what kind of error may occur
		catch(IndexOutOfBoundsException ie){ 
			 System.out.println("WRONG INDEXING\n");
			 throw new Error(ie.getMessage());
		}
		catch (FileNotFoundException fe) {
	         System.out.println("COULDN'T READ FILE\n");
	         throw new Error(fe.getMessage());
	    }
	      System.out.println("FILE READ SUCCESFULY!!\n");
	}
	
	public void fillDatabase(Databases d, List<String> fileContent){
		int counter = 0;
		int tableIndex = 0;
		for (String s : fileContent) {
			counter++;//track position of strings 
			if(s.equals(Symbols.TABLESYMBOL.getText())){
				d.createNewTable();
				tableIndex = d.getTables().size();//check how many tables already exist
				//minus one because array position starts at 0
				//get the string from correct position and set name
				d.getTables().get(tableIndex-1).setName(fileContent.get(counter));
			}
			if(s.equals(Symbols.COLUMNSYMBOL.getText())){
				//same as before
				d.getTables().get(tableIndex -1).setColumns(findColumnStrings(counter, fileContent));
			}
			if(s.equals(Symbols.RECORDSYMBOL.getText())){
				Tables currentTable = d.getTables().get(tableIndex-1);//same as before
				currentTable.getRows().add(Tables.createRecord(counter, fileContent));//add a record
			}
		}
	}
	
	//return a list of strings up until we meet a records or tables symbol
	public List<String> findColumnStrings(int index, List<String> fileContent){
		List<String> list = new ArrayList<>();
		for(int i = index; i< fileContent.size(); i++){
			if(!fileContent.get(i).equals(Symbols.RECORDSYMBOL.getText()) &&
			   !fileContent.get(i).equals(Symbols.TABLESYMBOL.getText())){
				list.add(fileContent.get(i));
			}
			else{
				break;
			}
		}
		return list;	
	}
	
	
	private void filesInDirectory(){
		try {
			//find directory address
	         File fileobject = new File("."); 
	         File[] fileArray = fileobject.listFiles();
	         for (File file : fileArray) {
	            if (file.isFile()) {
	               System.out.println(file.getPath());
	            }
	         }
	      }
	      catch (Exception e) {
	         System.out.println("	");
	         throw new Error(e);
	      }
		
	}
	
	public String convertListToStrings(List<String> list){
		String listContentAsString = "";
		for (String s : list) {
			listContentAsString += s + " ";
		}
		return listContentAsString;
	}
	
	private void printSavedFile(List<String> newFile){
		for (String s : newFile) {
			System.out.print(s);
		} 
	}

}
